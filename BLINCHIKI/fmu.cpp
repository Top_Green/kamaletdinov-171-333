// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "dmu.h"
#include "fmu.h"
#include "frame.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buBliniClick(TObject *Sender) {
	tc->GotoVisibleTab(tiBliniList->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	tc->TabPosition = TTabPosition::None;
	tc->GotoVisibleTab(tiMenu->Index);
}

// ---------------------------------------------------------------------------

void __fastcall TForm1::ListView1ItemClick(TObject * const Sender,
	TListViewItem * const AItem)

{
	laQuantity->Text = "1";
	tc->GotoVisibleTab(tiBlin->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button9Click(TObject *Sender) {
	tc->GotoVisibleTab(tiBliniList->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button10Click(TObject *Sender) {
	tc->GotoVisibleTab(tiMenu->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button11Click(TObject *Sender) {
	if (laQuantity->Text != "1") {
		laQuantity->Text = StrToInt(laQuantity->Text) - 1;
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button12Click(TObject *Sender) {
	laQuantity->Text = StrToInt(laQuantity->Text) + 1;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buAddBucketClick(TObject *Sender) {

	bucket.push_back({(double)dm->quBliniListID->Value,
			(double)StrToInt(laQuantity->Text), dm->quBliniListPRICE->Value});

	TFrame1 * x = new TFrame1(glBucket);
	x->Parent = glBucket;
	x->Align = TAlignLayout::Client;
	x->Name = "frame" + IntToStr(dm->quBliniListID->Value);
	x->laNameFrame->Text = dm->quBliniListNAME->Value;
	x->imFrame->Bitmap->Assign(dm->quBliniListIMAGE);
	x->laPriceFrame->Text = dm->quBliniListPRICE->Value;
	x->laQuantityFrame->Text = laQuantity->Text;

	glBucket->RecalcSize();

	tc->GotoVisibleTab(tiBucket->Index);

}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button7Click(TObject *Sender) {
	tc->GotoVisibleTab(tiBlin->Index);
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::Button13Click(TObject *Sender) {
	tc->GotoVisibleTab(tiMenu->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button8Click(TObject *Sender) {
	tc->GotoVisibleTab(tiOrder->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buOrderClick(TObject *Sender) {

	dm->OrderingIns(Random(10000), edName->Text, edPhone->Text, edEmail->Text,
		edAddress->Text, edDate->Text, edTime->Text, edAmount->Text,
		memoDesc->Text);

	int id = dm->quOrdersID->Value;

	for (int i = 0; i < bucket.size(); i++) {
		dm->OrderingListIns(id, bucket[i][0], bucket[i][1], bucket[i][2]);
	}

	tc->GotoVisibleTab(tiBliniList->Index);

}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender) {
	tc->GotoVisibleTab(tiBucket->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender) {
	tc->GotoVisibleTab(tiFeedbackList->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button14Click(TObject *Sender) {
	dm->FeedbackIns(edNameFeed->Text, edPhoneFeed->Text, edEmailFeed->Text,
		memoComment->Text);
	tc->GotoVisibleTab(tiFeedbackList->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button15Click(TObject *Sender) {
	tc->GotoVisibleTab(tiMenu->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button16Click(TObject *Sender) {
	tc->GotoVisibleTab(tiFeedback->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button17Click(TObject *Sender) {
	tc->GotoVisibleTab(tiFeedbackList->Index);
}

// ---------------------------------------------------------------------------

void __fastcall TForm1::Button6Click(TObject *Sender) {
	tc->GotoVisibleTab(tiContact->Index);
}
// ---------------------------------------------------------------------------
