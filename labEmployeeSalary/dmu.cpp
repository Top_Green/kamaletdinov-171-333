// ---------------------------------------------------------------------------

#pragma hdrstop

#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;

// ---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner) : TDataModule(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall Tdm::FDQuery1AfterScroll(TDataSet *DataSet) {
	FDQuery2->Filter = FDQuery2EMP_NO->FieldName + " = " +
		FDQuery1EMP_NO->Value;
}
// ---------------------------------------------------------------------------
