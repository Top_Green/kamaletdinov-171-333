// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
#include "uUtils.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender) {
	passwordEdit->Text = RandomStr(StrToIntDef(pasLength->Text, 9),
		lowerCaseCheck->IsChecked, upperCaseCheck->IsChecked,
		numbersCheck->IsChecked, specialCheck->IsChecked);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::infoBtnClick(TObject *Sender) {
	ShowMessage("qqq");
}
// ---------------------------------------------------------------------------
