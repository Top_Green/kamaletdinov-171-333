// ---------------------------------------------------------------------------

#pragma hdrstop

#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;

// ---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner) : TDataModule(Owner) {
}

void Tdm::OrderingListIns(int order_id, int product_id, double product_price,
	int quantity) {
	spOrderingListIns->ParamByName("ORDER_ID")->Value = order_id;
	spOrderingListIns->ParamByName("PRODUCT_ID")->Value = product_id;
	spOrderingListIns->ParamByName("PRODUCT_PRICE")->Value = product_price;
	spOrderingListIns->ParamByName("QUANTITY")->Value = quantity;

	spOrderingListIns->ExecProc();
}

void Tdm::OrderingIns(int id, UnicodeString fio, UnicodeString tel,
	UnicodeString email, UnicodeString address, UnicodeString date,
	UnicodeString time, UnicodeString sum, UnicodeString note) {

	spOrderIns->ParamByName("ID")->Value = id;
	spOrderIns->ParamByName("CLIENT_FIO")->Value = fio;
	spOrderIns->ParamByName("CLIENT_TEl")->Value = tel;
	spOrderIns->ParamByName("CLIENT_EMAIL")->Value = email;
	spOrderIns->ParamByName("CLIENT_ADDRESS")->Value = address;
	spOrderIns->ParamByName("DELIVERY_DATE")->Value = StrToDate(date);
	spOrderIns->ParamByName("DELIVERY_TIME")->Value = StrToTime(time);
	spOrderIns->ParamByName("DELIVERY_AMOUNT")->Value = (double)StrToInt(sum);
	spOrderIns->ParamByName("DELIVERY_NOTE")->Value = note;

	spOrderIns->ExecProc();
}

void Tdm::FeedbackIns(UnicodeString name, UnicodeString phone,
	UnicodeString email, UnicodeString comment) {

	spFeedbackIns->ParamByName("NAME")->Value = name;
	spFeedbackIns->ParamByName("PHONE")->Value = phone;
	spFeedbackIns->ParamByName("EMAIL")->Value = email;
	spFeedbackIns->ParamByName("COMMENT")->Value = comment;

	spFeedbackIns->ExecProc();

}

// ---------------------------------------------------------------------------
