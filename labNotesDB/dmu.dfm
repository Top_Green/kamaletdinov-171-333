object dm: Tdm
  OldCreateOrder = False
  Height = 346
  Width = 439
  object FDConnection: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Lenovo\Documents\mobile\testrepository\labNote' +
        'sDB\Notes.db'
      'DriverID=SQLite')
    LoginPrompt = False
    AfterConnect = FDConnectionAfterConnect
    BeforeConnect = FDConnectionBeforeConnect
    Left = 96
    Top = 56
  end
  object taNotes: TFDTable
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'Notes'
    TableName = 'Notes'
    Left = 88
    Top = 168
    object taNotesCaption: TStringField
      FieldName = 'Caption'
      Origin = 'Caption'
      Required = True
      Size = 50
    end
    object taNotesPriority: TSmallintField
      FieldName = 'Priority'
      Origin = 'Priority'
      Required = True
    end
    object taNotesDetail: TStringField
      FieldName = 'Detail'
      Origin = 'Detail'
      Size = 500
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 216
    Top = 64
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 224
    Top = 160
  end
end
