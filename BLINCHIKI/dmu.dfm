object dm: Tdm
  OldCreateOrder = False
  Height = 296
  Width = 383
  object spOrderingListIns: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'ORDERING_LIST_INS'
    Left = 264
    Top = 56
    ParamData = <
      item
        Position = 1
        Name = 'QUANTITY'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'PRODUCT_PRICE'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Position = 3
        Name = 'PRODUCT_ID'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 4
        Name = 'ORDER_ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 64
    Top = 216
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 168
    Top = 216
  end
  object quBliniList: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    product.id,'
      '    product.category_id,'
      '    product.name,'
      '    product.image,'
      '    product.gram,'
      '    product.kcal,'
      '    product.price,'
      '    product.note,'
      '    product.sticker'
      'from product'
      'order by product.category_id')
    Left = 168
    Top = 72
    object quBliniListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quBliniListCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Origin = 'CATEGORY_ID'
      Required = True
    end
    object quBliniListNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      Required = True
      Size = 90
    end
    object quBliniListIMAGE: TBlobField
      FieldName = 'IMAGE'
      Origin = 'IMAGE'
    end
    object quBliniListGRAM: TIntegerField
      FieldName = 'GRAM'
      Origin = 'GRAM'
    end
    object quBliniListKCAL: TIntegerField
      FieldName = 'KCAL'
      Origin = 'KCAL'
    end
    object quBliniListPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'PRICE'
      Required = True
    end
    object quBliniListNOTE: TWideStringField
      FieldName = 'NOTE'
      Origin = 'NOTE'
      Size = 4000
    end
    object quBliniListSTICKER: TIntegerField
      FieldName = 'STICKER'
      Origin = 'STICKER'
    end
  end
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\Lenovo\Documents\mobile\testrepository\BLINCHI' +
        'KI\db\BLINCHIKI.fdb'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    LoginPrompt = False
    Left = 56
    Top = 56
  end
  object quOrders: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    ordering.id,'
      '    ordering.create_datetime,'
      '    ordering.client_fio,'
      '    ordering.client_tel,'
      '    ordering.client_email,'
      '    ordering.client_address,'
      '    ordering.delivery_date,'
      '    ordering.delivery_time,'
      '    ordering.delivery_amount,'
      '    ordering.delivery_note,'
      '    ordering.status,'
      '    ordering.status_note'
      'from ordering'
      'order by ordering.create_datetime desc')
    Left = 216
    Top = 128
    object quOrdersID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quOrdersCREATE_DATETIME: TSQLTimeStampField
      FieldName = 'CREATE_DATETIME'
      Origin = 'CREATE_DATETIME'
      Required = True
    end
    object quOrdersCLIENT_FIO: TWideStringField
      FieldName = 'CLIENT_FIO'
      Origin = 'CLIENT_FIO'
      Size = 70
    end
    object quOrdersCLIENT_TEL: TWideStringField
      FieldName = 'CLIENT_TEL'
      Origin = 'CLIENT_TEL'
      Size = 15
    end
    object quOrdersCLIENT_EMAIL: TWideStringField
      FieldName = 'CLIENT_EMAIL'
      Origin = 'CLIENT_EMAIL'
      Size = 70
    end
    object quOrdersCLIENT_ADDRESS: TWideStringField
      FieldName = 'CLIENT_ADDRESS'
      Origin = 'CLIENT_ADDRESS'
      Size = 150
    end
    object quOrdersDELIVERY_DATE: TDateField
      FieldName = 'DELIVERY_DATE'
      Origin = 'DELIVERY_DATE'
    end
    object quOrdersDELIVERY_TIME: TTimeField
      FieldName = 'DELIVERY_TIME'
      Origin = 'DELIVERY_TIME'
    end
    object quOrdersDELIVERY_AMOUNT: TFloatField
      FieldName = 'DELIVERY_AMOUNT'
      Origin = 'DELIVERY_AMOUNT'
    end
    object quOrdersDELIVERY_NOTE: TWideStringField
      FieldName = 'DELIVERY_NOTE'
      Origin = 'DELIVERY_NOTE'
      Size = 4000
    end
    object quOrdersSTATUS: TIntegerField
      FieldName = 'STATUS'
      Origin = 'STATUS'
      Required = True
    end
    object quOrdersSTATUS_NOTE: TWideStringField
      FieldName = 'STATUS_NOTE'
      Origin = 'STATUS_NOTE'
      Size = 4000
    end
  end
  object quOrderList: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    ordering_list1.id,'
      '    ordering_list1.order_id,'
      '    ordering_list1.product_id,'
      '    ordering_list1.product_price,'
      '    ordering_list1.quantity'
      'from ordering_list ordering_list1')
    Left = 280
    Top = 136
    object quOrderListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object quOrderListORDER_ID: TIntegerField
      FieldName = 'ORDER_ID'
      Origin = 'ORDER_ID'
      Required = True
    end
    object quOrderListPRODUCT_ID: TIntegerField
      FieldName = 'PRODUCT_ID'
      Origin = 'PRODUCT_ID'
      Required = True
    end
    object quOrderListPRODUCT_PRICE: TFloatField
      FieldName = 'PRODUCT_PRICE'
      Origin = 'PRODUCT_PRICE'
      Required = True
    end
    object quOrderListQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
      Origin = 'QUANTITY'
      Required = True
    end
  end
  object spOrderIns: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'ORDERING_INS'
    Left = 64
    Top = 144
    ParamData = <
      item
        Position = 1
        Name = 'CLIENT_EMAIL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 2
        Name = 'CLIENT_TEL'
        DataType = ftWideString
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 3
        Name = 'CLIENT_FIO'
        DataType = ftWideString
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 4
        Name = 'CLIENT_ADDRESS'
        DataType = ftWideString
        ParamType = ptInput
        Size = 150
      end
      item
        Position = 5
        Name = 'DELIVERY_DATE'
        DataType = ftDate
        ParamType = ptInput
      end
      item
        Position = 6
        Name = 'DELIVERY_TIME'
        DataType = ftTime
        ParamType = ptInput
      end
      item
        Position = 7
        Name = 'DELIVERY_AMOUNT'
        DataType = ftFloat
        ParamType = ptInput
      end
      item
        Position = 8
        Name = 'DELIVERY_NOTE'
        DataType = ftWideString
        ParamType = ptInput
        Size = 4000
      end
      item
        Position = 9
        Name = 'ID'
        DataType = ftInteger
        ParamType = ptInput
      end>
  end
  object spFeedbackIns: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'FEEDBACK_INS'
    Left = 280
    Top = 192
    ParamData = <
      item
        Position = 1
        Name = 'COMMENT'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 1000
      end
      item
        Position = 2
        Name = 'EMAIL'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 70
      end
      item
        Position = 3
        Name = 'PHONE'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 15
      end
      item
        Position = 4
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 100
      end>
  end
  object quFeedback: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    feedback.comment,'
      '    feedback.name,'
      '    feedback.phone,'
      '    feedback.email'
      'from feedback')
    Left = 184
    Top = 24
    object quFeedbackCOMMENT: TWideStringField
      FieldName = 'COMMENT'
      Origin = '"COMMENT"'
      FixedChar = True
      Size = 1000
    end
    object quFeedbackNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      FixedChar = True
      Size = 100
    end
    object quFeedbackPHONE: TWideStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      FixedChar = True
      Size = 15
    end
    object quFeedbackEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Origin = 'EMAIL'
      FixedChar = True
      Size = 70
    end
  end
end
