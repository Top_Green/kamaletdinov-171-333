// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

inline int High(const UnicodeString &S) {
#ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
#else
	return S.Length() - 1;
#endif
}

inline int Low(const UnicodeString &S) {
#ifdef _DELPHI_STRING_ONE_BASED
	return 1;
#else
	return 0;
#endif
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {

	tabCtrl->First();
	tabCtrl->TabPosition = TTabPosition::None;
	backBtn->Visible = false;
	infoLbl->Text = L"��������";
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::startBtnClick(TObject *Sender) {
	StartGame();
	infoLbl->Text = category;
	tabCtrl->Next();
}

// ---------------------------------------------------------------------------
void TForm1::StartGame() {
	vector<UnicodeString>words;
	if (radio1->IsChecked) {
		category = L"��������";
		words = {
			L"��������", L"�����", L"��������", L"��������", L"�����",
			L"���������", L"������", L"������", L"��������", L"�������",
			L"������", L"�������", L"��������", L"���������", L"�������",
			L"�������", L"�������", L"�������", L"��������", L"��������",
			L"�����", L"������", L"�������", L"����"};
	}
	else if (radio2->IsChecked) {
		category = L"���������";
		words = {
			L"���������", L"���������", L"�����������", L"�����", L"�������",
			L"���������", L"��������", L"����������", L"������", L"��������",
			L"�����", L"���������", L"����ƨ�", L"��������", L"��������",
			L"���������", L"��������", L"�����������", L"��������", L"��������",
			L"��������", L"�����������", L"��������", L"�������������",
			L"���������"};
	}
	else if (radio3->IsChecked) {
		category = L"������";
		words = {
			L"������", L"��������", L"�������", L"�������", L"��������",
			L"������", L"�����������", L"�������", L"������", L"�������",
			L"�������", L"�����", L"������", L"��������", L"������", L"������",
			L"�������", L"������", L"�������", L"������", L"�Ш�������",
			L"�����", L"���������", L"����������"};
	}
	else if (radio4->IsChecked) {
		category = L"������";
		words = {
			L"��������", L"��������", L"�������", L"���������", L"������",
			L"�����������", L"���������", L"�����������", L"���������",
			L"��������", L"�������", L"��������", L"��������", L"����",
			L"������", L"������", L"������", L"���������", L"�����", L"����",
			L"������", L"����������", L"�������", L"��������", L"�����"};
	}

	word = words[Random(words.size())];
	wordCode = word;
	health = 0;
	Glyph->ImageIndex = health;
	for (int i = Low(wordCode); i <= High(wordCode); i++) {
		wordCode[i] = '*';
	}
	wordLbl->Text = wordCode;

	for (int i = 0; i < buttons.size(); i++) {
		buttons.at(i)->Visible = true;
	}

	buttons.clear();
	memo->Lines->Clear();

}

void __fastcall TForm1::ButtonClick(TObject *Sender) {

	buttons.push_back((TButton*)Sender);
	UnicodeString letter = ((TButton*)Sender)->Text;
	bool flag = false;

	for (int i = Low(wordCode); i <= High(wordCode); i++) {
		if ((UnicodeString)word[i] == letter) {
			wordCode[i] = word[i];
			flag = true;
		}
	}

	wordLbl->Text = wordCode;
	((TButton*)Sender)->Visible = false;

	if (!flag) {
		health++;
		Glyph->ImageIndex = health;
	}
	if (health == 6) {
		resultFormer();
		ShowMessage(L"�� ���������");
		tabCtrl->GotoVisibleTab(Result->Index);
	}
	if (wordCode == word) {
		ShowMessage(L"�� �������� ���������� �����!");
		wordsResult.push_back(word);
		StartGame();
	}

}

// ---------------------------------------------------------------------------
void __fastcall TForm1::backBtnClick(TObject *Sender) {
	infoLbl->Text = L"��������";
	tabCtrl->First();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::infoBtnClick(TObject *Sender) {
	ShowMessage(L"����������� ���� - ������������ �.�.(171-333)");
}

// ---------------------------------------------------------------------------
void TForm1::resultFormer() {
	infoLbl->Text = L"����������";
	memo->Lines->Add(L"���������� �����: ");
	for (int i = 0; i < wordsResult.size(); i++) {
		memo->Lines->Add(wordsResult[i]);
	}
	wordsResult.clear();
}

void __fastcall TForm1::rulesBtnClick(TObject *Sender) {
	infoLbl->Text = L"������� ����";
	tabCtrl->GotoVisibleTab(Rules->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::tabCtrlChange(TObject *Sender) {
	if (tabCtrl->ActiveTab->Index != Menu->Index) {
		backBtn->Visible = true;
	}
	else {
		backBtn->Visible = false;
	}
}
// ---------------------------------------------------------------------------
