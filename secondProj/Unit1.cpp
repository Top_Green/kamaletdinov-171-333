//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
    starCount = 0;
    marcoCount =0;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::StarImgClick(TObject *Sender)
{
	StarLbl->Text = "Star = " +IntToStr(++starCount);
    StarLblAni->Start();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MarcoImgClick(TObject *Sender)
{
	MarcoLbl->Text = "Marco = " +IntToStr(++marcoCount);
    MarcoLblAni->Start();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormResize(TObject *Sender)
{
	StarAni->StopValue = this->Width-StarImg->Width;
	MarcoAni->StopValue = this->Width-MarcoImg->Width;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::LudoImgClick(TObject *Sender)
{
	marcoCount = 0;
	starCount = 0;
	StarLbl->Text = "Star = 0";
	MarcoLbl->Text = "Marco = 0";
}
//---------------------------------------------------------------------------
