// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	tc->ActiveTab = tList;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormShow(TObject *Sender) {
	dm->FDConnection->Connected = true;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::addBtnClick(TObject *Sender) {
	dm->taNotes->Append();
	tc->GotoVisibleTab(tItem->Index);
}

// ---------------------------------------------------------------------------

void __fastcall TForm1::lvItemClick(TObject * const Sender,
	TListViewItem * const AItem)

{
	dm->taNotes->Edit();
	tc->GotoVisibleTab(tItem->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::saveBtnClick(TObject *Sender) {
	dm->taNotes->Post();
	tc->GotoVisibleTab(tItem->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::cancelBtnClick(TObject *Sender) {
	dm->taNotes->Cancel();
	tc->GotoVisibleTab(tList->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::deleteBtnClick(TObject *Sender) {
	dm->taNotes->Delete();
	tc->GotoVisibleTab(tList->Index);
}
// ---------------------------------------------------------------------------
