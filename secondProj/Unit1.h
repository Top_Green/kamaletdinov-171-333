//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLabel *StarLbl;
	TLabel *MarcoLbl;
	TImage *StarImg;
	TImage *MarcoImg;
	TFloatAnimation *StarAni;
	TFloatAnimation *MarcoAni;
	TFloatAnimation *StarLblAni;
	TFloatAnimation *MarcoLblAni;
	TImage *LudoImg;
	TFloatAnimation *LudoAni;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall StarImgClick(TObject *Sender);
	void __fastcall MarcoImgClick(TObject *Sender);
	void __fastcall FormResize(TObject *Sender);
	void __fastcall LudoImgClick(TObject *Sender);
private:	// User declarations
	int starCount;
	int marcoCount;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
