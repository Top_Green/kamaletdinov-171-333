//---------------------------------------------------------------------------

#ifndef formH
#define formH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TShadowEffect *ShadowEffect1;
	TShadowEffect *ShadowEffect2;
	TBlurEffect *BlurEffect1;
	TBlurEffect *BlurEffect2;
	TGlowEffect *GlowEffect1;
	TGlowEffect *GlowEffect2;
	TInnerGlowEffect *InnerGlowEffect1;
	TInnerGlowEffect *InnerGlowEffect2;
	TBevelEffect *BevelEffect1;
	TReflectionEffect *ReflectionEffect1;
	TReflectionEffect *ReflectionEffect2;
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
