// ---------------------------------------------------------------------------

#ifndef formH
#define formH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Colors.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.NumberBox.hpp>

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buNewImage;
	TButton *buNewRectangle;
	TButton *buClear;
	TButton *buAbout;
	TGlyph *Glyph1;
	TSelection *Selection1;
	TLayout *ly;
	TSelection *Selection2;
	TGlyph *Glyph2;
	TSelection *Selection3;
	TRectangle *Rectangle1;
	TToolBar *tbOptions;
	TButton *buBringToFront;
	TButton *buSendToBack;
	TTrackBar *trRotation;
	TButton *buDel;
	TToolBar *tbImage;
	TButton *buImagePrev;
	TButton *buImageNext;
	TButton *buImageSelect;
	TButton *buImageRand;
	TToolBar *tbRectangle;
	TComboColorBox *ComboColorBoxRect;
	TTrackBar *trRectRadius;
	TSelection *Selection4;
	TText *Text1;
	TToolBar *tbText;
	TEdit *edText;
	TNumberBox *numFontSize;
	TButton *buNewText;
	void __fastcall SelectionMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buBringToFrontClick(TObject *Sender);
	void __fastcall buSendToBackClick(TObject *Sender);
	void __fastcall trRotationChange(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buImagePrevClick(TObject *Sender);
	void __fastcall buImageNextClick(TObject *Sender);
	void __fastcall buImageSelectClick(TObject *Sender);
	void __fastcall buImageRandClick(TObject *Sender);
	void __fastcall ComboColorBoxRectChange(TObject *Sender);
	void __fastcall trRectRadiusChange(TObject *Sender);
	void __fastcall buNewImageClick(TObject *Sender);
	void __fastcall buNewRectangleClick(TObject *Sender);
	void __fastcall buClearClick(TObject *Sender);
	void __fastcall edTextChange(TObject *Sender);
	void __fastcall numFontSizeChange(TObject *Sender);
	void __fastcall buNewTextClick(TObject *Sender);

private: // User declarations
	TSelection *FSel;

	void SelectionAll(TObject *Sender);

public: // User declarations
	__fastcall TForm1(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
