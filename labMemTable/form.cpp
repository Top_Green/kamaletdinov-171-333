// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"

// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::saveBtnClick(TObject *Sender) {
	table->Post();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::cancelBtnClick(TObject *Sender) {
	table->Cancel();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::filterOnBtnClick(TObject *Sender) {
	table->Filter = filterEdit->Text;
	table->Filtered = true;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::filterOffBtnClick(TObject *Sender) {
	table->Filtered = false;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::saveFileBtnClick(TObject *Sender) {
	table->SaveToFile(Ioutils::TPath::GetDocumentsPath() + PathDelim +
		fileNameEdit->Text + ".json");
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::loadFileBtnClick(TObject *Sender) {
	table->LoadFromFile(Ioutils::TPath::GetDocumentsPath() + PathDelim +
		fileNameEdit->Text + ".json");
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::addAgeBtnClick(TObject *Sender) {
	table->Edit();
	tableAge->Value += 1;
	table->Post();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::addMoneyClick(TObject *Sender) {
	table->First();
	while (!table->Eof) {
		table->Edit();
		tableMoney->Value += 100;
		table->Post();
		table->Next();
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::readOnlyGridChange(TObject *Sender) {
	Grid1->ReadOnly = readOnlyGrid->IsChecked;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::readOnlyTableChange(TObject *Sender) {
	table->ReadOnly = readOnlyTable->IsChecked;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::locateBtnClick(TObject *Sender) {
	table->Locate(tableAge->FieldName, lEdit->Text);
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::lookupBtnClick(TObject *Sender) {
	ShowMessage(table->Lookup(tableAge->FieldName, lEdit->Text,
		tableFIO->FieldName));
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::sumBtnClick(TObject *Sender) {
	int sum = 0;
	table->First();
	while (!table->Eof) {
		sum += tableMoney->Value; ;
		table->Next();
	}
	ShowMessage(sum);
}
// ---------------------------------------------------------------------------
