//---------------------------------------------------------------------------

#ifndef FormH
#define FormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.WebBrowser.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TEdit *UrlEdit;
	TButton *SearchBtn;
	TToolBar *ToolBar2;
	TButton *StopBtn;
	TButton *RefreshBtn;
	TButton *NextBtn;
	TButton *BackBtn;
	TButton *InfoBtn;
	TWebBrowser *WebBrowser;
	void __fastcall SearchBtnClick(TObject *Sender);
	void __fastcall BackBtnClick(TObject *Sender);
	void __fastcall NextBtnClick(TObject *Sender);
	void __fastcall RefreshBtnClick(TObject *Sender);
	void __fastcall StopBtnClick(TObject *Sender);
	void __fastcall UrlEditKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall WebBrowserDidFinishLoad(TObject *ASender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall InfoBtnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
