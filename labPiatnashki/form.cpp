// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	for (int i = 0; i <= 15; i++) {
		FListGlyph[i] =
			(TGlyph*)(this->FindComponent("Glyph" + IntToStr(i + 1)));
	}
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key,
	System::WideChar &KeyChar, TShiftState Shift) {

	if (Key == VK_RIGHT) {
		for (int i = 0; i <= 15; i++) {
			if (FListGlyph[i]->ImageIndex == -1 && i % 4 != 0) {
				FListGlyph[i]->ImageIndex = FListGlyph[i - 1]->ImageIndex;
				FListGlyph[i - 1]->ImageIndex = -1;
				break;
			}
		}
	}
	if (Key == vkLeft) {
		for (int i = 0; i <= 15; i++) {
			if (FListGlyph[i]->ImageIndex == -1 && i % 4 != 3) {
				FListGlyph[i]->ImageIndex = FListGlyph[i + 1]->ImageIndex;
				FListGlyph[i + 1]->ImageIndex = -1;
				break;
			}
		}
	}
	if (Key == vkUp) {
		for (int i = 0; i <= 15; i++) {
			if (FListGlyph[i]->ImageIndex == -1 && i < 12) {
				FListGlyph[i]->ImageIndex = FListGlyph[i + 4]->ImageIndex;
				FListGlyph[i + 4]->ImageIndex = -1;
				break;
			}
		}
	}
	if (Key == vkDown) {
		for (int i = 0; i <= 15; i++) {
			if (FListGlyph[i]->ImageIndex == -1 && i > 3) {
				FListGlyph[i]->ImageIndex = FListGlyph[i - 4]->ImageIndex;
				FListGlyph[i - 4]->ImageIndex = -1;
				break;
			}
		}
	}
}

// ---------------------------------------------------------------------------

