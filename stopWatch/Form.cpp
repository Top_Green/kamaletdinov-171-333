//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Form.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::StartBtnClick(TObject *Sender)
{
    FTimeStart = Now();
	Timer->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::StopBtnClick(TObject *Sender)
{
	Timer->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TimerTimer(TObject *Sender)
{
    TimerLabel->Text = TimeToStr(Now() - FTimeStart);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::RoundBtnClick(TObject *Sender)
{
    RoundText->Lines->Add(TimerLabel->Text);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ClearBtnClick(TObject *Sender)
{
    RoundText->Lines->Clear();
}
//---------------------------------------------------------------------------
