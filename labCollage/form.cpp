// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void TForm1::SelectionAll(TObject * Sender) {
	if (FSel != NULL) {
		FSel->HideSelection = true;
	}
	FSel = dynamic_cast<TSelection*>(Sender);
	if (FSel != NULL) {
		FSel->HideSelection = false;
	}
	//
	tbOptions->Visible = (FSel) != NULL;
	if (tbOptions->Visible) {
		trRotation->Value = FSel->RotationAngle;
	}
	tbImage->Visible = (FSel != NULL) && dynamic_cast<TGlyph*>
		(FSel->Controls->Items[0]);
	tbRectangle->Visible = (FSel != NULL) && dynamic_cast<TRectangle*>
		(FSel->Controls->Items[0]);
	tbText->Visible = (FSel != NULL) && dynamic_cast<TText*>
		(FSel->Controls->Items[0]);
	if (tbRectangle->Visible) {
		ComboColorBoxRect->Color = ((TRectangle*)FSel->Controls->Items[0])
			->Fill->Color;
		trRectRadius->Value = ((TRectangle*)FSel->Controls->Items[0])->XRadius;
	}
	if (tbText->Visible) {
		edText->Text = ((TText*)FSel->Controls->Items[0])->Text;
		numFontSize->Value = ((TText*)FSel->Controls->Items[0])
			->TextSettings->Font->Size;
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::SelectionMouseDown(TObject *Sender, TMouseButton Button,
	TShiftState Shift, float X, float Y) {
	SelectionAll(Sender);
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender) {
	FSel = NULL;
	SelectionAll(ly);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buBringToFrontClick(TObject *Sender) {
	FSel->BringToFront();
	FSel->Repaint();
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::buSendToBackClick(TObject *Sender) {
	FSel->SendToBack();
	FSel->Repaint();
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::trRotationChange(TObject *Sender) {
	FSel->RotationAngle = trRotation->Value;
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::buDelClick(TObject *Sender) {
	FSel->DisposeOf();
	FSel = NULL;
	SelectionAll(ly);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buImagePrevClick(TObject *Sender) {
	TGlyph *x = (TGlyph*)FSel->Controls->Items[0];
	x->ImageIndex = ((int)x->ImageIndex <= 0) ? dm->il->Count - 1 :
		(int)x->ImageIndex - 1;
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::buImageNextClick(TObject *Sender) {
	TGlyph *x = (TGlyph*)FSel->Controls->Items[0];
	x->ImageIndex = ((int)x->ImageIndex >= dm->il->Count) ? 0 :
		(int)x->ImageIndex + 1;
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::buImageSelectClick(TObject *Sender) {
	//
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::buImageRandClick(TObject *Sender) {
	TGlyph *x = (TGlyph*)FSel->Controls->Items[0];
	x->ImageIndex = Random(dm->il->Count);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::ComboColorBoxRectChange(TObject *Sender) {
	TRectangle *x = (TRectangle*)FSel->Controls->Items[0];
	x->Fill->Color = ComboColorBoxRect->Color;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::trRectRadiusChange(TObject *Sender) {
	TRectangle *x = (TRectangle*)FSel->Controls->Items[0];
	x->XRadius = trRectRadius->Value;
	x->YRadius = x->XRadius;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buNewImageClick(TObject *Sender) {
	TSelection *x = new TSelection(ly);
	x->Parent = ly;
	x->GripSize = 5;
	x->Width = 50 + Random(100);
	x->Height = 50 + Random(100);
	x->Position->X = Random(ly->Width - x->Width);
	x->Position->Y = Random(ly->Height - x->Height);
	x->RotationAngle = Random(100) - 50;
	x->OnMouseDown = SelectionMouseDown;
	TGlyph *xGlyph = new TGlyph(x);
	xGlyph->Parent = x;
	xGlyph->Align = TAlignLayout::Client;
	xGlyph->Images = dm->il;
	xGlyph->ImageIndex = Random(dm->il->Count);

	SelectionAll(x);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buNewRectangleClick(TObject *Sender) {
	TSelection *x = new TSelection(ly);
	x->Parent = ly;
	x->GripSize = 5;
	x->Width = 50 + Random(100);
	x->Height = 50 + Random(100);
	x->Position->X = Random(ly->Width - x->Width);
	x->Position->Y = Random(ly->Height - x->Height);
	x->RotationAngle = Random(100) - 50;
	x->OnMouseDown = SelectionMouseDown;
	TRectangle *xRect = new TRectangle(x);
	xRect->Parent = x;
	xRect->Align = TAlignLayout::Client;
	xRect->HitTest = false;
	xRect->XRadius = Random(50);
	xRect->YRadius = xRect->XRadius;
	// xRect->Fill->Color = TAlphaColorF::Create(Random(256), Random(256),
	// Random(256));

	SelectionAll(x);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buClearClick(TObject *Sender) {
	SelectionAll(ly);
	for (int i = ly->ControlsCount - 1; i >= 0; i--) {
		if (dynamic_cast<TSelection*>(ly->Controls->Items[i])) {
			ly->RemoveObject(i);
		}
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::edTextChange(TObject *Sender) {
	TText *x = (TText*)FSel->Controls->Items[0];
	x->Text = edText->Text;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::numFontSizeChange(TObject *Sender) {
	TText *x = (TText*)FSel->Controls->Items[0];
	x->TextSettings->Font->Size = numFontSize->Value;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::buNewTextClick(TObject *Sender) {
	TSelection *x = new TSelection(ly);
	x->Parent = ly;
	x->GripSize = 5;
	x->Width = 50 + Random(100);
	x->Height = 50 + Random(100);
	x->Position->X = Random(ly->Width - x->Width);
	x->Position->Y = Random(ly->Height - x->Height);
	x->RotationAngle = Random(100) - 50;
	x->OnMouseDown = SelectionMouseDown;
	TText *xText = new TText(x);
	xText->Parent= x;
	xText->Align = TAlignLayout::Client;
	xText->HitTest = false;
	xText->Text = IntToStr(Random(50));
	xText->TextSettings->Font->Size = Random(50);

	SelectionAll(x);
}
// ---------------------------------------------------------------------------
