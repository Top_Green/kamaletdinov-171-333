// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {

	for (int i = 1; i <= 12; i++) {
		blocks[i] = (TGlyph*)(this->FindComponent("block" + IntToStr(i)));
	}
	tc->GotoVisibleTab(tiMenu->Index);

}

void TForm1::GameStart() {

	scoreLbl->Text = "0";
	tm->Enabled = true;
	playerIndex = playerSkin->ImageIndex;
	for (int i = 1; i <= 12; i++) {
		if (i != 11) {
			blocks[i]->ImageIndex = 0;
		}
		else {
			blocks[i]->ImageIndex = playerIndex;
		}
	}
	isGameOver = false;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormKeyDown(TObject * Sender, WORD & Key,
	System::WideChar & KeyChar, TShiftState Shift) {
	if (Key == vkRight && blocks[12]->ImageIndex != playerIndex) {
		if (blocks[10]->ImageIndex == playerIndex) {
			int x = blocks[11]->ImageIndex;
			blocks[10]->ImageIndex = 0;
			blocks[11]->ImageIndex = playerIndex;
			playerCol = 1;

			if (x != 0) {
				GameOver();
			}

		}
		else if (blocks[11]->ImageIndex == playerIndex) {

			int x = blocks[12]->ImageIndex;
			blocks[11]->ImageIndex = 0;
			blocks[12]->ImageIndex = playerIndex;
			playerCol = 2;

			if (x != 0) {
				GameOver();
			}
		}

	}
	if (Key == vkLeft && blocks[10]->ImageIndex != playerIndex) {
		if (blocks[12]->ImageIndex == playerIndex) {
			int x = blocks[11]->ImageIndex;
			blocks[12]->ImageIndex = 0;
			blocks[11]->ImageIndex = playerIndex;
			playerCol = 1;

			if (x != 0) {
				GameOver();
			}

		}
		else if (blocks[11]->ImageIndex == playerIndex) {
			int x = blocks[10]->ImageIndex;
			blocks[11]->ImageIndex = 0;
			blocks[10]->ImageIndex = playerIndex;
			playerCol = 0;

			if (x != 0) {
				GameOver();
			}
		}
	}
}

void TForm1::GameOver() {
	tm->Enabled = false;

	for (int i = 10; i <= 12; i++) {
		if (blocks[i]->ImageIndex == playerIndex) {
			blocks[i]->ImageIndex = 1;
		}
	}
	score = StrToInt(scoreLbl->Text);
	dm->quPlayer->Close();
	dm->quPlayer->ParamByName("name")->AsString = name;
	dm->quPlayer->Open();
	refreshBtn->Visible = true;
	isGameOver = true;
	if (dm->quPlayerNAME->Value != "") {
		if (dm->quPlayerSCORE->Value < score) {
			dm->playerUpd(name, score);
		}
	}
	else {
		dm->playerIns(name, score);
	}
}

void TForm1::randomObstacle() {

	int columnNumber = Random(3);
	do {
		blocks[columnNumber + 1]->ImageIndex = Random(9) + 2;
	}
	while (blocks[columnNumber + 1]->ImageIndex == playerIndex);

}

void TForm1::moveBlocks() {
	for (int i = 12; i >= 4; i--) {
		if (blocks[i]->ImageIndex != playerIndex) {
			blocks[i]->ImageIndex = blocks[i - 3]->ImageIndex;
		}
		else if (blocks[i - 3]->ImageIndex != 0) {
			GameOver();
		}
	}
	for (int i = 1; i <= 3; i++) {
		blocks[i]->ImageIndex = 0;
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::tmTimer(TObject * Sender) {
	moveBlocks();
	scoreLbl->Text =
		IntToStr(StrToInt(scoreLbl->Text) + StrToInt(5000 / tm->Interval));
	if (Random(4) != 0) {
		randomObstacle();
	}

}

// ---------------------------------------------------------------------------
void __fastcall TForm1::refreshBtnClick(TObject * Sender) {
	GameStart();
	refreshBtn->Visible = false;
}

// ---------------------------------------------------------------------------

void __fastcall TForm1::RecordsClick(TObject * Sender) {

	dm->quLeaderboard->Close();
	dm->quLeaderboard->Open();
	tc->GotoVisibleTab(tiResult->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::StartClick(TObject * Sender) {
	tc->GotoVisibleTab(tiGame->Index);
	name = edName->Text;
	GameStart();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::backBtnClick(TObject * Sender) {
	if (isGameOver) {
		tc->GotoVisibleTab(tiMenu->Index);
	}
	else {
		GameOver();
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::edNameChange(TObject * Sender) {
	if (edName->Text != "") {
		Start->Enabled = true;
	}
	else {
		Start->Enabled = false;
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::SettingsClick(TObject * Sender) {

	playerSkin->ImageIndex = tbPlayerSkin->Value;
	tc->GotoVisibleTab(tiSettings->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::tbDifficultyChange(TObject * Sender) {
	tm->Interval = -tbDifficulty->Value;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormActivate(TObject *Sender) {
	tbDifficulty->Value = dm->quSettingsDIFFICULTY->Value;
	tbPlayerSkin->Value = dm->quSettingsSKIN->Value;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action) {
	dm->settingsUpd(tbDifficulty->Value, tbPlayerSkin->Value);
}
// ---------------------------------------------------------------------------
