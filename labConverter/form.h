//---------------------------------------------------------------------------

#ifndef formH
#define formH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TButton *backBtn;
	TButton *infoBtn;
	TLabel *Label1;
	TTabControl *tabCtrl;
	TTabItem *menuTab;
	TTabItem *timeTab;
	TTabItem *distanceTab;
	TTabItem *dataTab;
	TTabItem *tempTab;
	TGridPanelLayout *GridPanelLayout1;
	TButton *timeBtn;
	TButton *distanceBtn;
	TButton *dateBtn;
	TButton *tempBtn;
	TListBox *ListBox1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TEdit *secEdit;
	TListBox *ListBox2;
	TEdit *mmEdit;
	TListBoxItem *ListBoxItem6;
	TListBoxItem *ListBoxItem7;
	TListBoxItem *ListBoxItem8;
	TListBox *ListBox3;
	TListBoxItem *ListBoxItem9;
	TEdit *Edit3;
	TListBoxItem *ListBoxItem10;
	TListBoxItem *ListBoxItem11;
	TListBoxItem *ListBoxItem12;
	TListBox *ListBox4;
	TListBoxItem *ListBoxItem13;
	TEdit *Edit4;
	TListBoxItem *ListBoxItem14;
	TListBoxItem *ListBoxItem15;
	TListBoxItem *ListBoxItem16;
	TEdit *minEdit;
	TEdit *hourEdit;
	TEdit *dayEdit;
	TEdit *smEdit;
	TEdit *mEdit;
	TEdit *kmEdit;
	TEdit *Edit11;
	TEdit *Edit12;
	TEdit *Edit13;
	TEdit *Edit17;
	TEdit *Edit14;
	TEdit *Edit15;
	TListBoxItem *ListBoxItem17;
	TEdit *msEdit;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall distanceBtnClick(TObject *Sender);
	void __fastcall dateBtnClick(TObject *Sender);
	void __fastcall tempBtnClick(TObject *Sender);
	void __fastcall backBtnClick(TObject *Sender);
	void __fastcall tabCtrlChange(TObject *Sender);
	void __fastcall timeBtnClick(TObject *Sender);
	void __fastcall timeEditKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall distanceEditKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
