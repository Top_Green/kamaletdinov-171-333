//---------------------------------------------------------------------------

#ifndef formH
#define formH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Controls3D.hpp>
#include <FMX.Layers3D.hpp>
#include <System.Math.Vectors.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TLabel *Label1;
	TButton *infoBtn;
	TTabControl *tabCtrl;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem7;
	TButton *startBtn;
	TImage *Image1;
	TLabel *Label2;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TScrollBox *ScrollBox1;
	TButton *Button5;
	TScrollBox *ScrollBox2;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TLabel *Label3;
	TButton *Button10;
	TScrollBox *ScrollBox3;
	TButton *Button11;
	TButton *Button12;
	TButton *Button13;
	TButton *Button14;
	TLabel *Label4;
	TButton *Button15;
	TButton *restartBtn;
	TMemo *memo;
	TLabel *questionLbl;
	TProgressBar *pb;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TTabItem *TabItem5;
	TScrollBox *ScrollBox4;
	TButton *Button16;
	TButton *Button17;
	TButton *Button18;
	TButton *Button19;
	TLabel *Label5;
	TButton *Button20;
	TImage *Image5;
	TImage *Image6;
	TTabItem *TabItem6;
	TScrollBox *ScrollBox5;
	TButton *Button21;
	TButton *Button22;
	TButton *Button23;
	TButton *Button24;
	TLabel *Label6;
	TButton *Button25;
	TImage *Image7;
	TLayout3D *Layout3D1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall startBtnClick(TObject *Sender);
	void __fastcall ButtonClick(TObject *Sender);
	void __fastcall restartBtnClick(TObject *Sender);
	void __fastcall tabCtrlChange(TObject *Sender);
	void __fastcall infoBtnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
