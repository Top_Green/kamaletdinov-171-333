//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Form.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TForm1::Restart(){
	rightStat =0;
	wrongStat=0;
	Continue();
}
//---------------------------------------------------------------------------
void TForm1::Continue(){

	rightLbl->Text = Format(L"Right = %d", ARRAYOFCONST((rightStat)));
	wrongLbl->Text = Format(L"Wrong = %d", ARRAYOFCONST((wrongStat)));

	int aValue = random(range);
	int bValue = random(range);
	int sign=(random(2)==1)?-1:1;
	int result = aValue+bValue;
	int newResult = random(2)==1? result: result+random(7)*sign;

	isCorrect = result==newResult;

	expressionLbl->Text = Format("%d + %d = %d",
	 ARRAYOFCONST((aValue, bValue, newResult)));


}
//---------------------------------------------------------------------------
void TForm1::Answer(bool value){
	(value == isCorrect)? rightStat++:wrongStat++;
    Continue();
}
void __fastcall TForm1::yesBtnClick(TObject *Sender)
{
	Answer(true);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::noBtnClick(TObject *Sender)
{
    Answer(false);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::restartBtnClick(TObject *Sender)
{
    Restart();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::infoBtnClick(TObject *Sender)
{
	ShowMessage("message");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize();
    Restart();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::difficultyBtnClick(TObject *Sender)
{
	isHard=!isHard;

	if(isHard){
		range = 100;
		difficultyBtn->Text = "Easy";
	} else {
		range = 20;
 		difficultyBtn->Text = "Hard";
	}
	Restart();
}
//---------------------------------------------------------------------------

