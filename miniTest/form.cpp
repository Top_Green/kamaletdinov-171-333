//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tabCtrl->First();
	tabCtrl->TabPosition=TTabPosition::None;
	pb->Max = tabCtrl->TabCount-2;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::startBtnClick(TObject *Sender)
{
	memo->Lines->Clear();
	tabCtrl->Next();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonClick(TObject *Sender)
{
	UnicodeString string;
	string = ((TControl*)Sender)->Tag==1?L": �����":L": �������";
	memo->Lines->Add(L"������ "+tabCtrl->ActiveTab->Text+string);
	tabCtrl->Next();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::restartBtnClick(TObject *Sender)
{
	tabCtrl->First();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::tabCtrlChange(TObject *Sender)
{
	pb->Value = tabCtrl->ActiveTab->Index;

	if(tabCtrl->ActiveTab->Index==(tabCtrl->TabCount-1) || tabCtrl->ActiveTab->Index==0){
		questionLbl->Text = L"";
	} else {
		questionLbl->Text = L"[" + IntToStr(tabCtrl->ActiveTab->Index) + L" �� " +
		(tabCtrl->TabCount-2) +L"]";
	}
}


//---------------------------------------------------------------------------

void __fastcall TForm1::infoBtnClick(TObject *Sender)
{
    ShowMessage("��� ���������� ���� ����������� �����");
}
//---------------------------------------------------------------------------

