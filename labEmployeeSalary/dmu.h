//---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDConnection *FDConnection1;
	TFDTable *FDTable1;
	TFDQuery *FDQuery1;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDQuery *FDQuery2;
	TSmallintField *FDQuery1EMP_NO;
	TStringField *FDQuery1LAST_NAME;
	TStringField *FDQuery1FIRST_NAME;
	TStringField *FDQuery1PHONE_EXT;
	TSQLTimeStampField *FDQuery1HIRE_DATE;
	TFMTBCDField *FDQuery1SALARY;
	TStringField *FDQuery1DEPT_NO;
	TStringField *FDQuery1DEPARTMENT;
	TSmallintField *FDQuery2EMP_NO;
	TSQLTimeStampField *FDQuery2CHANGE_DATE;
	TFMTBCDField *FDQuery2OLD_SALARY;
	TFloatField *FDQuery2NEW_SALARY;
	void __fastcall FDQuery1AfterScroll(TDataSet *DataSet);
private:	// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
