object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 252
  Width = 349
  object FDConnection1: TFDConnection
    Params.Strings = (
      'User_Name=SYSDBA'
      'Password=masterkey'
      
        'Database=C:\Users\Lenovo\Documents\mobile\testrepository\road\db' +
        '\TEST.FDB'
      'CharacterSet=UTF8'
      'DriverID=FB')
    LoginPrompt = False
    AfterConnect = FDConnection1AfterConnect
    BeforeConnect = FDConnection1BeforeConnect
    Left = 24
    Top = 16
  end
  object quLeaderboard: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    players.name,'
      '    players.score'
      'from players'
      'order by score desc')
    Left = 224
    Top = 32
    object quLeaderboardNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 15
    end
    object quLeaderboardSCORE: TIntegerField
      FieldName = 'SCORE'
      Origin = 'SCORE'
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 160
    Top = 192
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 272
    Top = 184
  end
  object spPlayerIns: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'PLAYER_INS'
    Left = 32
    Top = 104
    ParamData = <
      item
        Position = 1
        Name = 'SCORE'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 15
      end>
  end
  object quPlayer: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    players.name,'
      '    players.score'
      'from players'
      'where('
      'name = :name'
      ')')
    Left = 168
    Top = 24
    ParamData = <
      item
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 15
      end>
    object quPlayerNAME: TWideStringField
      FieldName = 'NAME'
      Origin = 'NAME'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 15
    end
    object quPlayerSCORE: TIntegerField
      FieldName = 'SCORE'
      Origin = 'SCORE'
    end
  end
  object spPlayerUpd: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'PLAYER_UPD'
    Left = 32
    Top = 160
    ParamData = <
      item
        Position = 1
        Name = 'SCORE'
        DataType = ftInteger
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'NAME'
        DataType = ftFixedWideChar
        ParamType = ptInput
        Size = 15
      end>
  end
  object spSettingsUpd: TFDStoredProc
    Connection = FDConnection1
    StoredProcName = 'SETTINGS_UPD'
    Left = 104
    Top = 104
    ParamData = <
      item
        Position = 1
        Name = 'DIFFICULTY'
        DataType = ftSingle
        ParamType = ptInput
      end
      item
        Position = 2
        Name = 'SKIN'
        DataType = ftSmallint
        ParamType = ptInput
      end>
  end
  object quSettings: TFDQuery
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      '    settings.difficulty,'
      '    settings.skin'
      'from settings')
    Left = 224
    Top = 96
    object quSettingsSKIN: TSmallintField
      FieldName = 'SKIN'
      Origin = 'SKIN'
    end
    object quSettingsDIFFICULTY: TSingleField
      FieldName = 'DIFFICULTY'
      Origin = 'DIFFICULTY'
    end
  end
end
