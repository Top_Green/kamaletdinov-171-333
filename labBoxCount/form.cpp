// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
#include "Unit1.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender) {
	tc->TabPosition = TTabPosition::None;
	tc->First();
	FListBox = new TList;
	for (int i = 1; i <= maxBox; i++) {
		FListBox->Add(this->FindComponent("Rectangle" + IntToStr(i)));
	}
	FListAns = new TList;
	for (int i = 1; i <= maxAns; i++) {
		FListAns->Add(this->FindComponent("ansBtn" + IntToStr(i)));
	}
	DoReset();

}
// ---------------------------------------------------------------------------

void __fastcall TForm1::FormDestroy(TObject *Sender) {
	delete FListBox;
	delete FListAns;
}

// ---------------------------------------------------------------------------
void TForm1::DoReset() {
	FCountCorrect = 0;
	FCountWrong = 0;

	FTimerValue = Time().Val + (double)(30 / (24 * 60 * 60));
	tm->Enabled = true;
	DoContinue();
}

void TForm1::DoContinue() {

	for (int i = 0; i < maxBox; i++) {
		((TRectangle*)FListBox->Items[i])->Fill->Color =
			TAlphaColorRec::Lightgray;
	}
	//

	FNumberCorrect = RandomRange(minPossible, maxPossible);

	int *x = RandomArrayUnique(maxBox, FNumberCorrect);

	for (int i = 0; i < FNumberCorrect; i++) {
		((TRectangle*)FListBox->Items[x[i]])->Fill->Color =
			TAlphaColorRec::Lightgreen;
	}
	colorFlag = Random(2);
	if (colorFlag == 1) {
		FNumberCorrect = maxBox - FNumberCorrect;
		qLbl->Text = "������� ����� ���������?";
	}
	else {
		qLbl->Text = "������� ������� ���������?";
	}

	int xAnsStart = FNumberCorrect - Random(maxAns - 1);
	if (xAnsStart < minPossible) {
		xAnsStart = minPossible;
	}
	//
	for (int i = 0; i < maxAns; i++) {
		((TButton*)FListAns->Items[i])->Text = IntToStr(xAnsStart + i);
	}
}

void TForm1::DoAnswer(int aValue) {

	(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
	if (FCountWrong > 5) {
		DoFinish();
	}
	DoContinue();
}

void TForm1::DoFinish() {
	tm->Enabled = false;
	tc->GotoVisibleTab(Result->Index);
	memo->Lines->Add("���������� ������ �������: " + IntToStr(FCountCorrect));
}

void __fastcall TForm1::ansBtnClick(TObject *Sender) {
	DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::tmTimer(TObject *Sender) {
	double x = FTimerValue - Time().Val;
	timeLbl->Text = FormatDateTime("nn:ss", x);
	if (x <= 0) {
		// DoFinish();
	}
}
// ---------------------------------------------------------------------------

void __fastcall TForm1::resetBtnClick(TObject *Sender) {
	DoReset();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::startBtnClick(TObject *Sender) {
	tc->Next();

}

// ---------------------------------------------------------------------------
void __fastcall TForm1::backBtnClick(TObject *Sender) {
	tc->First();
	DoReset();
}
// ---------------------------------------------------------------------------
