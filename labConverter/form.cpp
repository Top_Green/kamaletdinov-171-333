// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	tabCtrl->First();
	tabCtrl->TabPosition = TTabPosition::None;
}
// ---------------------------------------------------------------------------

// ---------------------------------------------------------------------------
void __fastcall TForm1::distanceBtnClick(TObject *Sender) {
	tabCtrl->GotoVisibleTab(distanceTab->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::dateBtnClick(TObject *Sender) {
	tabCtrl->GotoVisibleTab(dataTab->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::tempBtnClick(TObject *Sender) {
	tabCtrl->GotoVisibleTab(tempTab->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::backBtnClick(TObject *Sender) {
	tabCtrl->First();
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::tabCtrlChange(TObject *Sender) {
	//
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::timeBtnClick(TObject *Sender) {
	tabCtrl->GotoVisibleTab(timeTab->Index);
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::timeEditKeyUp(TObject *Sender, WORD &Key,
	System::WideChar &KeyChar, TShiftState Shift) {
	long double x;
	long double xSec;

	x = StrToFloatDef(((TEdit*)Sender)->Text, 0);
	switch (((TEdit*)Sender)->Tag) {

	case 0:
		xSec = x / 1000;
		break;
	case 1:
		xSec = x;
		break;
	case 2:
		xSec = x * 60;
		break;
	case 3:
		xSec = x * 60 * 60;
		break;
	case 4:
		xSec = x * 60 * 60 * 24;
		break;
	}

	msEdit->Text = FloatToStr(xSec * 1000);
	secEdit->Text = FloatToStr(xSec);
	minEdit->Text = FloatToStr(xSec / 60);
	hourEdit->Text = FloatToStr(xSec / 60 / 60);
	dayEdit->Text = FloatToStr(xSec / 60 / 60 / 24);

}
// ---------------------------------------------------------------------------
void __fastcall TForm1::distanceEditKeyUp(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
    long double x;
	long double xSec;

	x = StrToFloatDef(((TEdit*)Sender)->Text, 0);
	switch (((TEdit*)Sender)->Tag) {

	case 0:
		xSec = x / 1000;
		break;
	case 1:
		xSec = x / 100;
		break;
	case 2:
		xSec = x;
		break;
	case 3:
		xSec = x * 1000;
		break;
	}

	mmEdit->Text = FloatToStr(xSec * 1000);
	smEdit->Text = FloatToStr(xSec * 100);
	mEdit->Text = FloatToStr(xSec);
	kmEdit->Text = FloatToStr(xSec /1000);

}
//---------------------------------------------------------------------------

