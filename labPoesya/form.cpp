// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

inline int High(const UnicodeString &S) {
#ifdef _DELPHI_STRING_ONE_BASED
	return S.Length();
#else
	return S.Length() - 1
#endif
}

inline int Low(const UnicodeString &S) {
#ifdef _DELPHI_STRING_ONE_BASED
	return 1;
#else
	return 0;
#endif
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender) {
	tabCtrl->First();

	UnicodeString x;

	// ����� �������
	for (int i = 0; i < meFull->Lines->Count; i++) {
		x = meFull->Lines->Strings[i];
		if (i % 2 == 1) {
			for (int j = Low(x); j <= High(x); j++) {
				if (x[j] != ' ') {
					x[j] = 'x';
				}
			}
		}
		me1->Lines->Add(x);
	}

	// ������ �����
	bool xFlag;

	for (int i = 0; i < meFull->Lines->Count; i++) {
		x = meFull->Lines->Strings[i];
		xFlag = false;
		for (int j = Low(x); j <= High(x); j++) {
			if ((xFlag) && (x[j] != ' '))
				x[j] = 'x';
			if ((!xFlag) && (x[j] == ' '))
				xFlag = true;
		}
		me2->Lines->Add(x);
	}

	// ������ ����� �����

	for (int i = 0; i < meFull->Lines->Count; i++) {
		x = meFull->Lines->Strings[i];
		for (int j = Low(x); j <= High(x); j++) {
			if ((j != Low(x)) && (x[j] != ' '))
				x[j] = 'x';
		}
		me3->Lines->Add(x);
	}

	//

	for (int i = 0; i < meFull->Lines->Count; i++) {
		x = meFull->Lines->Strings[i];
		xFlag = true;
		for (int j = Low(x); j <= High(x); j++) {
			if (x[j] == ' ') {
				xFlag = true;
			}
			if (xFlag && x[j] != ' ') {
				xFlag = false;
			}
			else if (x[j] != ' ')
				x[j] = 'x';
		}
		me4->Lines->Add(x);
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::sizeOutBtnClick(TObject * Sender) {
	meFull->TextSettings->Font->Size -= 2;
	me1->TextSettings->Font->Size -= 2;
	me2->TextSettings->Font->Size -= 2;
	me3->TextSettings->Font->Size -= 2;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::sizeInBtnClick(TObject * Sender) {
	meFull->TextSettings->Font->Size += 2;
	me1->TextSettings->Font->Size += 2;
	me2->TextSettings->Font->Size += 2;
	me3->TextSettings->Font->Size += 2;
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::infoBtnClick(TObject * Sender) {
	ShowMessage("hello");
}
// ---------------------------------------------------------------------------
