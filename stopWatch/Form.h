//---------------------------------------------------------------------------

#ifndef FormH
#define FormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TLabel *TimerLabel;
	TButton *StartBtn;
	TButton *StopBtn;
	TTimer *Timer;
	TButton *RoundBtn;
	TMemo *RoundText;
	TButton *ClearBtn;
	void __fastcall StartBtnClick(TObject *Sender);
	void __fastcall StopBtnClick(TObject *Sender);
	void __fastcall TimerTimer(TObject *Sender);
	void __fastcall RoundBtnClick(TObject *Sender);
	void __fastcall ClearBtnClick(TObject *Sender);
private:	// User declarations
    TDateTime FTimeStart;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
