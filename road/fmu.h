// ---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <System.ImageList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <System.Rtti.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.Grid.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <System.Bindings.Outputs.hpp>
#include <FMX.Edit.hpp>

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiGame;
	TTabItem *tiResult;
	TImageList *il;
	TGridPanelLayout *GridPanelLayout1;
	TGlyph *block1;
	TGlyph *block2;
	TGlyph *block3;
	TGlyph *block4;
	TGlyph *block5;
	TGlyph *block6;
	TGlyph *block7;
	TGlyph *block8;
	TGlyph *block9;
	TGlyph *block10;
	TGlyph *block11;
	TGlyph *block12;
	TTimer *tm;
	TToolBar *ToolBar1;
	TButton *refreshBtn;
	TButton *backBtn;
	TLabel *scoreLbl;
	TButton *Start;
	TButton *Settings;
	TButton *Records;
	TGridPanelLayout *GridPanelLayout2;
	TLayout *Layout1;
	TButton *Button1;
	TLabel *Label1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkGridToDataSource *LinkGridToDataSourceBindSourceDB1;
	TEdit *edName;
	TLabel *Label2;
	TGrid *Grid;
	TTabItem *tiSettings;
	TGlyph *playerSkin;
	TToolBar *ToolBar2;
	TTrackBar *tbPlayerSkin;
	TToolBar *ToolBar3;
	TButton *Button2;
	TLinkControlToProperty *LinkControlToPropertyImageIndex;
	TLabel *Label3;
	TLabel *Label4;
	TTrackBar *tbDifficulty;
	TStyleBook *StyleBook1;

	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
		System::WideChar &KeyChar, TShiftState Shift);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall refreshBtnClick(TObject *Sender);
	void __fastcall RecordsClick(TObject *Sender);
	void __fastcall StartClick(TObject *Sender);
	void __fastcall backBtnClick(TObject *Sender);
	void __fastcall edNameChange(TObject *Sender);
	void __fastcall SettingsClick(TObject *Sender);
	void __fastcall tbDifficultyChange(TObject *Sender);
	void __fastcall FormActivate(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);


private: // User declarations
		int score = 0;

	UnicodeString name;

	int lastRand = 1;
	int playerCol = 1;

	int playerIndex = 4;

	int boobIndex;
	int speed;

	bool isGameOver = true;

public: // User declarations

	TGlyph* blocks[12];

	void randomObstacle();
	void randomTime();
	void moveBlocks();
	void GameOver();
	void GameStart();

	__fastcall TForm1(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
