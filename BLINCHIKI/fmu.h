// ---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Objects.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Edit.hpp>

#include <vector>

// ---------------------------------------------------------------------------
using namespace std;

class TForm1 : public TForm {
__published: // IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiBliniList;
	TTabItem *tiBlin;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TButton *buBlini;
	TImage *Image1;
	TListView *ListView1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TToolBar *ToolBar1;
	TLabel *Blinch;
	TLabel *laName;
	TGridPanelLayout *GridPanelLayout2;
	TButton *buAddBucket;
	TButton *buAddOrder;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *Label2;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *laWeight;
	TLabel *laKcal;
	TLabel *laPrice;
	TLinkPropertyToField *LinkPropertyToFieldText;
	TLinkPropertyToField *LinkPropertyToFieldText2;
	TLinkPropertyToField *LinkPropertyToFieldText3;
	TMemo *memo;
	TImage *imBlin;
	TLinkControlToField *LinkControlToField1;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TLinkPropertyToField *LinkPropertyToFieldText4;
	TToolBar *ToolBar2;
	TButton *Button9;
	TToolBar *ToolBar3;
	TButton *Button10;
	TLabel *Label1;
	TTabItem *tiBucket;
	TToolBar *ToolBar4;
	TGridPanelLayout *GridPanelLayout4;
	TButton *Button11;
	TLabel *laQuantity;
	TButton *Button12;
	TTabItem *tiOrder;
	TGridLayout *glBucket;
	TButton *Button7;
	TLabel *�������;
	TButton *Button8;
	TGridPanelLayout *GridPanelLayout6;
	TLabel *Label5;
	TEdit *edName;
	TLabel *Label6;
	TEdit *edPhone;
	TLabel *Label7;
	TEdit *edEmail;
	TLabel *Label8;
	TEdit *edAddress;
	TLabel *Label9;
	TEdit *edDate;
	TLabel *Label10;
	TEdit *edTime;
	TLabel *Label11;
	TEdit *edAmount;
	TLabel *Label12;
	TMemo *memoDesc;
	TToolBar *ToolBar5;
	TButton *Button13;
	TButton *buOrder;
	TTabItem *tiFeedback;
	TToolBar *ToolBar6;
	TGridPanelLayout *GridPanelLayout5;
	TLabel *���;
	TEdit *edNameFeed;
	TLabel *�������;
	TEdit *edPhoneFeed;
	TLabel *Email;
	TEdit *edEmailFeed;
	TLabel *�����;
	TMemo *memoComment;
	TButton *Button14;
	TTabItem *tiFeedbackList;
	TToolBar *ToolBar7;
	TListView *ListView2;
	TButton *Button15;
	TButton *Button16;
	TBindSourceDB *BindSourceDB2;
	TLinkListControlToField *LinkListControlToField2;
	TButton *Button17;
	TTabItem *tiContact;
	TLabel *Label13;

	void __fastcall buBliniClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall ListView1ItemClick(TObject * const Sender,
		TListViewItem * const AItem);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
	void __fastcall Button11Click(TObject *Sender);
	void __fastcall Button12Click(TObject *Sender);
	void __fastcall buAddBucketClick(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button13Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall buOrderClick(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button14Click(TObject *Sender);
	void __fastcall Button15Click(TObject *Sender);
	void __fastcall Button16Click(TObject *Sender);
	void __fastcall Button17Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);

private: // User declarations

public: // User declarations
	__fastcall TForm1(TComponent* Owner);
	vector < vector < double >> bucket;

};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
