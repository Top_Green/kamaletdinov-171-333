// ---------------------------------------------------------------------------

#ifndef formH
#define formH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <vector>

using std::vector;

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TToolBar *ToolBar1;
	TTabControl *tabCtrl;
	TTabItem *Menu;
	TTabItem *Game;
	TTabItem *Result;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TButton *Button10;
	TButton *Button11;
	TButton *Button12;
	TButton *Button13;
	TButton *Button14;
	TButton *Button15;
	TButton *Button16;
	TButton *Button17;
	TButton *Button18;
	TButton *Button19;
	TButton *Button20;
	TButton *Button21;
	TButton *Button22;
	TButton *Button23;
	TButton *Button24;
	TButton *Button25;
	TButton *Button26;
	TButton *Button27;
	TButton *Button28;
	TButton *Button29;
	TButton *Button30;
	TButton *Button31;
	TButton *Button32;
	TButton *Button33;
	TLabel *wordLbl;
	TImageList *ImageList1;
	TGlyph *Glyph;
	TRadioButton *radio1;
	TRadioButton *radio2;
	TRadioButton *radio3;
	TRadioButton *radio4;
	TLabel *Label1;
	TButton *backBtn;
	TLabel *infoLbl;
	TButton *infoBtn;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout2;
	TButton *startBtn;
	TMemo *memo;
	TButton *rulesBtn;
	TTabItem *Rules;
	TMemo *Memo1;
	TStyleBook *StyleBook1;

	void __fastcall FormCreate(TObject *Sender);
	void __fastcall startBtnClick(TObject *Sender);
	void __fastcall ButtonClick(TObject *Sender);
	void __fastcall backBtnClick(TObject *Sender);
	void __fastcall infoBtnClick(TObject *Sender);
	void __fastcall rulesBtnClick(TObject *Sender);
	void __fastcall tabCtrlChange(TObject *Sender);

private: // User declarations
	void StartGame();
	void resultFormer();

	UnicodeString word;
	UnicodeString wordCode;
	int health;
    vector<UnicodeString> wordsResult;
	vector<TButton*>buttons;
	UnicodeString category;

public: // User declarations
	__fastcall TForm1(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
