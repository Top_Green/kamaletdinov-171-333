// ---------------------------------------------------------------------------

#ifndef formH
#define formH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TTabControl *tc;
	TTabItem *Menu;
	TTabItem *Game;
	TTabItem *Result;
	TTabItem *Settings;
	TLayout *Layout1;
	TButton *resetBtn;
	TLabel *qLbl;
	TLabel *timeLbl;
	TGridPanelLayout *GridPanelLayout1;
	TButton *ansBtn1;
	TButton *ansBtn2;
	TButton *ansBtn3;
	TButton *ansBtn4;
	TButton *ansBtn5;
	TButton *ansBtn6;
	TLayout *Layout2;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TRectangle *Rectangle4;
	TRectangle *Rectangle5;
	TRectangle *Rectangle6;
	TRectangle *Rectangle7;
	TRectangle *Rectangle8;
	TRectangle *Rectangle9;
	TRectangle *Rectangle10;
	TRectangle *Rectangle11;
	TRectangle *Rectangle12;
	TRectangle *Rectangle13;
	TRectangle *Rectangle14;
	TRectangle *Rectangle15;
	TRectangle *Rectangle16;
	TRectangle *Rectangle17;
	TRectangle *Rectangle18;
	TRectangle *Rectangle19;
	TRectangle *Rectangle20;
	TRectangle *Rectangle21;
	TRectangle *Rectangle22;
	TRectangle *Rectangle23;
	TRectangle *Rectangle24;
	TRectangle *Rectangle25;
	TTimer *tm;
	TButton *startBtn;
	TButton *settingsBtn;
	TLayout *Layout3;
	TButton *backBtn;
	TMemo *memo;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormDestroy(TObject *Sender);
	void __fastcall ansBtnClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
	void __fastcall resetBtnClick(TObject *Sender);
	void __fastcall startBtnClick(TObject *Sender);
	void __fastcall backBtnClick(TObject *Sender);

private: // User declarations
	int FCountCorrect;
	int FCountWrong;
	int FNumberCorrect;
	double FTimerValue;
    int colorFlag = 0;
	TList *FListBox;
	TList *FListAns;

	void DoReset();
	void DoContinue();
	void DoAnswer(int aValue);
	void DoFinish();

public: // User declarations
	__fastcall TForm1(TComponent* Owner);
};

const int maxBox = 25;
const int maxAns = 6;
const int minPossible = 4;
const int maxPossible = 14;
// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
