// ---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.IBBase.hpp>

// ---------------------------------------------------------------------------
class Tdm : public TDataModule {
__published: // IDE-managed Components
	TFDConnection *FDConnection1;
	TFDQuery *quLeaderboard;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TWideStringField *quLeaderboardNAME;
	TIntegerField *quLeaderboardSCORE;
	TFDStoredProc *spPlayerIns;
	TFDQuery *quPlayer;
	TFDStoredProc *spPlayerUpd;
	TWideStringField *quPlayerNAME;
	TIntegerField *quPlayerSCORE;
	TFDQuery *quSettings;
	TSmallintField *quSettingsSKIN;
	TSingleField *quSettingsDIFFICULTY;
	TFDStoredProc *spSettingsUpd;
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall FDConnection1BeforeConnect(TObject *Sender);
	void __fastcall FDConnection1AfterConnect(TObject *Sender);

private: // User declarations
public: // User declarations
	void playerIns(UnicodeString name, int score);
	void playerUpd(UnicodeString, int score);
	void settingsUpd(float difficulty, int skin);
	__fastcall Tdm(TComponent* Owner);

};

// ---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
// ---------------------------------------------------------------------------
#endif
