//---------------------------------------------------------------------------

#ifndef FormH
#define FormH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *restartBtn;
	TButton *infoBtn;
	TGridPanelLayout *GridPanelLayout1;
	TButton *yesBtn;
	TButton *noBtn;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TLabel *rightLbl;
	TLabel *wrongLbl;
	TLabel *questionLbl;
	TLabel *expressionLbl;
	TButton *difficultyBtn;
	void __fastcall yesBtnClick(TObject *Sender);
	void __fastcall noBtnClick(TObject *Sender);
	void __fastcall restartBtnClick(TObject *Sender);
	void __fastcall infoBtnClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall difficultyBtnClick(TObject *Sender);
private:	// User declarations
	int rightStat;
	int wrongStat;
	bool isHard;
    int range = 20;
	bool isCorrect;
	void Restart();
	void Continue();
    void Answer(bool value);
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
