//---------------------------------------------------------------------------

#ifndef formH
#define formH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *infoBtn;
	TLabel *Label1;
	TLayout *Layout1;
	TEdit *passwordEdit;
	TButton *Button1;
	TCheckBox *lowerCaseCheck;
	TCheckBox *specialCheck;
	TCheckBox *numbersCheck;
	TCheckBox *upperCaseCheck;
	TNumberBox *pasLength;
	TLabel *Label2;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall infoBtnClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
