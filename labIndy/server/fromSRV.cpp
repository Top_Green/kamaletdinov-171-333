// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fromSRV.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::startBtnClick(TObject *Sender) {
	IdTCPServer->Active = true;
	memo->Lines->Add("Active = true");
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::stopBtnClick(TObject *Sender) {
	IdTCPServer->Active = false;
	memo->Lines->Add("Active = false");
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::IdTCPServerConnect(TIdContext *AContext) {
	memo->Lines->Add(Format("[%s] - Client connected",
		ARRAYOFCONST((AContext->Connection->Socket->Binding->PeerIP))));
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::IdTCPServerDisconnect(TIdContext *AContext) {
	memo->Lines->Add(Format("[%s] - Client disconnected",
		ARRAYOFCONST((AContext->Connection->Socket->Binding->PeerIP))));
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::IdTCPServerExecute(TIdContext *AContext) {
	UnicodeString x = AContext->Connection->Socket->ReadLn();
	memo->Lines->Add("Input = " + x);
	//
	if (x == "time") {
		AContext->Connection->Socket->WriteLn(TimeToStr(Now()));
	}
	if (x == "str") {
		AContext->Connection->Socket->WriteLn(strEdit->Text,
			IndyTextEncoding_UTF8());
	}
	if (x == "image") {
		TMemoryStream *x = new TMemoryStream();
		try {
			img->Bitmap->SaveToStream(x);
			x->Seek(0, 0);
			AContext->Connection->Socket->Write(x->Size);
			AContext->Connection->Socket->Write(x);
		}
		__finally {
			delete x;
		}
	}
	if (x == "file") {
		TMemoryStream *x = new TMemoryStream();
		try {
			x->LoadFromFile("file.txt");
            x->Seek(0,0);
            AContext->Connection->Socket->Write(x->Size);
			AContext->Connection->Socket->Write(x);
		}
		__finally {
			delete x;
		}
	}

}

// ---------------------------------------------------------------------------
