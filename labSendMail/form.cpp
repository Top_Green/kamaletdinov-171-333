// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "form.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

// ---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm1::sendBtnClick(TObject *Sender) {
	IdSMTP->Host = "smtp.mail.ru";
	IdSMTP->Port = 587;
	IdSMTP->Username = "test123210@mail.ru";
	IdSMTP->Password = "Pa$$w0rd";
	IdSMTP->UseTLS = utUseExplicitTLS;
	IdSMTP->ReadTimeout = 15000;
	IdSMTP->Connect();

	if (IdSMTP->Connected() == true) {
		TIdMessage *x = new TIdMessage();
		try {
			x->From->Address = IdSMTP->Username;
			x->Recipients->Add()->Address = Trim(toEd->Text);
			x->Subject = Trim(subjEd->Text);
			x->Body->Assign(memoBody->Lines);
			x->CharSet = "Windows-1251";
			if (includeCheck->IsChecked) {
				UnicodeString xFileName =
					Ioutils::TPath::ChangeExtension
					(Ioutils::TPath::GetTempFileName(), ".txt");
				memoBody->Lines->SaveToFile(xFileName, TEncoding::UTF8);
				new TIdAttachmentFile(x->MessageParts, xFileName);
			}

			IdSMTP->Send(x);
			ShowMessage(L"������ ����������");
		}
		__finally {
			delete x;
			IdSMTP->Disconnect();
		}
	}
}
// ---------------------------------------------------------------------------
