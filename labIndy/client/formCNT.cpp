// ---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "formCNT.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm2 *Form2;

// ---------------------------------------------------------------------------
__fastcall TForm2::TForm2(TComponent* Owner) : TForm(Owner) {
}

// ---------------------------------------------------------------------------
void __fastcall TForm2::acConnectExecute(TObject *Sender) {
	IdTCPClient->Host = hostEdit->Text;
	IdTCPClient->Port = StrToInt(portEdit->Text);
	IdTCPClient->Connect();
}

// ---------------------------------------------------------------------------
void __fastcall TForm2::acDisconnectExecute(TObject *Sender) {
	IdTCPClient->Disconnect();
}

// ---------------------------------------------------------------------------
void __fastcall TForm2::acGetTimeExecute(TObject *Sender) {
	IdTCPClient->Socket->WriteLn("time");
	UnicodeString x;
	x = IdTCPClient->Socket->ReadLn();
	memo->Lines->Add(x);
}

// ---------------------------------------------------------------------------
void __fastcall TForm2::acGetStrExecute(TObject *Sender) {
	IdTCPClient->Socket->WriteLn("str");
	UnicodeString x;
	x = IdTCPClient->Socket->ReadLn(IndyTextEncoding_UTF8());
	memo->Lines->Add(x);
}

// ---------------------------------------------------------------------------
void __fastcall TForm2::acGetImgExecute(TObject *Sender) {
	IdTCPClient->Socket->WriteLn("image");
	TMemoryStream *x = new TMemoryStream();
	try {
		int xSize = IdTCPClient->Socket->ReadInt64();
		IdTCPClient->Socket->ReadStream(x, xSize);
		img->Bitmap->LoadFromStream(x);
//		x = IdTCPClient->Socket->ReadLn();
	}
	__finally {
		delete x;
	}
}

// ---------------------------------------------------------------------------
void __fastcall TForm2::alUpdate(TBasicAction *Action, bool &Handled) {
	hostEdit->Enabled = !IdTCPClient->Connected();
	portEdit->Enabled = !IdTCPClient->Connected();
	acConnect->Enabled = !IdTCPClient->Connected();
	acDisconnect->Enabled = IdTCPClient->Connected();
	acGetTime->Enabled = IdTCPClient->Connected();
	acGetStr->Enabled = IdTCPClient->Connected();
	acGetImg->Enabled = IdTCPClient->Connected();
}
// ---------------------------------------------------------------------------
void __fastcall TForm2::acGetFileExecute(TObject *Sender)
{
	IdTCPClient->Socket->WriteLn("file");
	TMemoryStream *x = new TMemoryStream();
	try {
		int xSize = IdTCPClient->Socket->ReadInt64();
		IdTCPClient->Socket->ReadStream(x, xSize);
		x->SaveToFile("file.txt");
        memo->Lines->Add("file");
	}
	__finally {
		delete x;
	}
}
//---------------------------------------------------------------------------

