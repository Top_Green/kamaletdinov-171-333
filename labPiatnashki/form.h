// ---------------------------------------------------------------------------

#ifndef formH
#define formH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Types.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TGridPanelLayout *GridPanelLayout1;
	TGlyph *Glyph1;
	TImageList *il;
	TGlyph *Glyph2;
	TGlyph *Glyph3;
	TGlyph *Glyph4;
	TGlyph *Glyph5;
	TGlyph *Glyph6;
	TGlyph *Glyph7;
	TGlyph *Glyph8;
	TGlyph *Glyph9;
	TGlyph *Glyph10;
	TGlyph *Glyph11;
	TGlyph *Glyph12;
	TGlyph *Glyph13;
	TGlyph *Glyph14;
	TGlyph *Glyph15;
	TGlyph *Glyph16;

	void __fastcall FormCreate(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
		System::WideChar &KeyChar, TShiftState Shift);

private: // User declarations
	// TList *FListGlyph;
	TGlyph *FListGlyph[16];

	void randomStart();

public: // User declarations
	__fastcall TForm1(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
