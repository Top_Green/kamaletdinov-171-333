// ---------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.Comp.UI.hpp>
#include <FireDAC.DApt.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.FMXUI.Wait.hpp>
#include <FireDAC.Phys.FB.hpp>
#include <FireDAC.Phys.FBDef.hpp>
#include <FireDAC.Phys.hpp>
#include <FireDAC.Phys.IBBase.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Async.hpp>
#include <FireDAC.Stan.Def.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.Pool.hpp>
#include <FireDAC.UI.Intf.hpp>

// ---------------------------------------------------------------------------
class Tdm : public TDataModule {
__published: // IDE-managed Components
	TFDStoredProc *spOrderingListIns;
	TFDGUIxWaitCursor *FDGUIxWaitCursor1;
	TFDPhysFBDriverLink *FDPhysFBDriverLink1;
	TFDQuery *quBliniList;
	TFDConnection *FDConnection1;
	TIntegerField *quBliniListID;
	TIntegerField *quBliniListCATEGORY_ID;
	TWideStringField *quBliniListNAME;
	TBlobField *quBliniListIMAGE;
	TIntegerField *quBliniListGRAM;
	TIntegerField *quBliniListKCAL;
	TFloatField *quBliniListPRICE;
	TWideStringField *quBliniListNOTE;
	TIntegerField *quBliniListSTICKER;
	TFDQuery *quOrders;
	TFDQuery *quOrderList;
	TIntegerField *quOrderListID;
	TIntegerField *quOrderListORDER_ID;
	TIntegerField *quOrderListPRODUCT_ID;
	TFloatField *quOrderListPRODUCT_PRICE;
	TIntegerField *quOrderListQUANTITY;
	TIntegerField *quOrdersID;
	TSQLTimeStampField *quOrdersCREATE_DATETIME;
	TWideStringField *quOrdersCLIENT_FIO;
	TWideStringField *quOrdersCLIENT_TEL;
	TWideStringField *quOrdersCLIENT_EMAIL;
	TWideStringField *quOrdersCLIENT_ADDRESS;
	TDateField *quOrdersDELIVERY_DATE;
	TTimeField *quOrdersDELIVERY_TIME;
	TFloatField *quOrdersDELIVERY_AMOUNT;
	TWideStringField *quOrdersDELIVERY_NOTE;
	TIntegerField *quOrdersSTATUS;
	TWideStringField *quOrdersSTATUS_NOTE;
	TFDStoredProc *spOrderIns;
	TFDStoredProc *spFeedbackIns;
	TFDQuery *quFeedback;
	TWideStringField *quFeedbackCOMMENT;
	TWideStringField *quFeedbackNAME;
	TWideStringField *quFeedbackPHONE;
	TWideStringField *quFeedbackEMAIL;

private: // User declarations
public: // User declarations
	__fastcall Tdm(TComponent* Owner);
	void OrderingListIns(int order_id, int product_id, double product_price,
		int quantity);
	void FeedbackIns(UnicodeString name, UnicodeString phone,
		UnicodeString email, UnicodeString comment);
	void OrderingIns(int id, UnicodeString fio, UnicodeString tel,
		UnicodeString email, UnicodeString address, UnicodeString date,
		UnicodeString time, UnicodeString sum, UnicodeString note);
};

// ---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
// ---------------------------------------------------------------------------
#endif
