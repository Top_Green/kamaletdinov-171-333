//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Form.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::SearchBtnClick(TObject *Sender)
{
	WebBrowser->URL = UrlEdit->Text;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::BackBtnClick(TObject *Sender)
{
	WebBrowser->GoBack();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::NextBtnClick(TObject *Sender)
{
	WebBrowser->GoForward();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::RefreshBtnClick(TObject *Sender)
{
	WebBrowser->Reload();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::StopBtnClick(TObject *Sender)
{
	WebBrowser->Stop();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::UrlEditKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
	if(Key == vkReturn){
		WebBrowser->URL = UrlEdit->Text;
	}

}
//---------------------------------------------------------------------------
void __fastcall TForm1::WebBrowserDidFinishLoad(TObject *ASender)
{
    UrlEdit->Text = WebBrowser->URL;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
    WebBrowser->URL = UrlEdit->Text;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::InfoBtnClick(TObject *Sender)
{
    ShowMessage("labWebBrowser - Kamaletdinov Artem (171-333)");
}
//---------------------------------------------------------------------------
