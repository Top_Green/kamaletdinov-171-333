// ---------------------------------------------------------------------------

#pragma hdrstop

#include "dmu.h"
// ---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;

// ---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner) : TDataModule(Owner) {
}
// ---------------------------------------------------------------------------

void Tdm::playerIns(UnicodeString name, int score) {
	spPlayerIns->ParamByName("NAME")->Value = name;
	spPlayerIns->ParamByName("SCORE")->Value = score;
	spPlayerIns->ExecProc();
}

void Tdm::playerUpd(UnicodeString name, int score) {
	spPlayerUpd->ParamByName("NAME")->Value = name;
	spPlayerUpd->ParamByName("SCORE")->Value = score;
	spPlayerUpd->ExecProc();
}

void Tdm::settingsUpd(float difficulty, int skin) {
	spSettingsUpd->ParamByName("DIFFICULTY")->Value = difficulty;
	spSettingsUpd->ParamByName("skin")->Value = skin;
	spSettingsUpd->ExecProc();
}

void __fastcall Tdm::DataModuleCreate(TObject *Sender) {
	FDConnection1->Connected = true;
}

// ---------------------------------------------------------------------------
void __fastcall Tdm::FDConnection1BeforeConnect(TObject *Sender) {
	FDConnection1->Params->Values["DATABASE"] = "..\\..\\db\\TEST.FDB";
}

// ---------------------------------------------------------------------------
void __fastcall Tdm::FDConnection1AfterConnect(TObject *Sender) {
	quLeaderboard->Open();
	quSettings->Open();
	quPlayer->Open();
}
// ---------------------------------------------------------------------------
