object dm: Tdm
  OldCreateOrder = False
  Height = 241
  Width = 317
  object FDConnection1: TFDConnection
    Params.Strings = (
      
        'Database=C:\Program Files\Firebird\Firebird_3_0\examples\empbuil' +
        'd\EMPLOYEE.FDB'
      'User_Name=SYSDBA'
      'Password=masterkey'
      'CharacterSet=UTF8'
      'DriverID=FB')
    Connected = True
    Left = 120
    Top = 128
  end
  object FDTable1: TFDTable
    Connection = FDConnection1
    Left = 32
    Top = 24
  end
  object FDQuery1: TFDQuery
    Active = True
    AfterScroll = FDQuery1AfterScroll
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      'employee.emp_no,'
      'employee.last_name,'
      'employee.first_name,'
      'employee.phone_ext,'
      'employee.hire_date,'
      'employee.salary,'
      'employee.dept_no,'
      'department.department'
      'from department'
      'inner join employee on (department.dept_no = employee.dept_no)'
      'order by employee.first_name, employee.last_name')
    Left = 208
    Top = 48
    object FDQuery1EMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      Required = True
    end
    object FDQuery1LAST_NAME: TStringField
      FieldName = 'LAST_NAME'
      Origin = 'LAST_NAME'
      Required = True
    end
    object FDQuery1FIRST_NAME: TStringField
      FieldName = 'FIRST_NAME'
      Origin = 'FIRST_NAME'
      Required = True
      Size = 15
    end
    object FDQuery1PHONE_EXT: TStringField
      FieldName = 'PHONE_EXT'
      Origin = 'PHONE_EXT'
      Size = 4
    end
    object FDQuery1HIRE_DATE: TSQLTimeStampField
      FieldName = 'HIRE_DATE'
      Origin = 'HIRE_DATE'
      Required = True
    end
    object FDQuery1SALARY: TFMTBCDField
      FieldName = 'SALARY'
      Origin = 'SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
    object FDQuery1DEPT_NO: TStringField
      FieldName = 'DEPT_NO'
      Origin = 'DEPT_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
      FixedChar = True
      Size = 3
    end
    object FDQuery1DEPARTMENT: TStringField
      FieldName = 'DEPARTMENT'
      Origin = 'DEPARTMENT'
      Required = True
      Size = 25
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 32
    Top = 112
  end
  object FDPhysFBDriverLink1: TFDPhysFBDriverLink
    Left = 88
    Top = 56
  end
  object FDQuery2: TFDQuery
    Active = True
    Filtered = True
    Connection = FDConnection1
    SQL.Strings = (
      'select '
      'salary_history.emp_no,'
      'salary_history.change_date,'
      'salary_history.old_salary,'
      'salary_history.new_salary'
      'from salary_history'
      'order by salary_history.change_date')
    Left = 200
    Top = 128
    object FDQuery2EMP_NO: TSmallintField
      FieldName = 'EMP_NO'
      Origin = 'EMP_NO'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQuery2CHANGE_DATE: TSQLTimeStampField
      FieldName = 'CHANGE_DATE'
      Origin = 'CHANGE_DATE'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object FDQuery2OLD_SALARY: TFMTBCDField
      FieldName = 'OLD_SALARY'
      Origin = 'OLD_SALARY'
      Required = True
      Precision = 18
      Size = 2
    end
    object FDQuery2NEW_SALARY: TFloatField
      FieldName = 'NEW_SALARY'
      Origin = 'NEW_SALARY'
    end
  end
end
