// ---------------------------------------------------------------------------

#ifndef formH
#define formH
// ---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.Grid.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.StorageBin.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <Fmx.Bind.Grid.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Grid.hpp>
#include <FMX.Grid.Style.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
#include <Data.Bind.Controls.hpp>
#include <Fmx.Bind.Navigator.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FireDAC.Stan.StorageJSON.hpp>
#include <System.IOUtils.hpp>

// ---------------------------------------------------------------------------
class TForm1 : public TForm {
__published: // IDE-managed Components
	TFDMemTable *table;
	TStringField *tableFIO;
	TStringField *tableTel;
	TIntegerField *tableAge;
	TCurrencyField *tableMoney;
	TGrid *Grid1;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkGridToDataSource *LinkGridToDataSourceBindSourceDB1;
	TBindNavigator *NavigatorBindSourceDB1;
	TEdit *Edit1;
	TEdit *Edit2;
	TEdit *Edit3;
	TEdit *Edit4;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
	TLinkControlToField *LinkControlToField3;
	TLinkControlToField *LinkControlToField4;
	TButton *saveBtn;
	TButton *cancelBtn;
	TEdit *filterEdit;
	TButton *filterOnBtn;
	TButton *filterOffBtn;
	TEdit *fileNameEdit;
	TButton *saveFileBtn;
	TButton *loadFileBtn;
	TFDStanStorageJSONLink *FDStanStorageJSONLink1;
	TButton *addAgeBtn;
	TButton *addMoney;
	TCheckBox *readOnlyGrid;
	TCheckBox *readOnlyTable;
	TEdit *lEdit;
	TButton *locateBtn;
	TButton *lookupBtn;
	TButton *sumBtn;

	void __fastcall saveBtnClick(TObject *Sender);
	void __fastcall cancelBtnClick(TObject *Sender);
	void __fastcall filterOnBtnClick(TObject *Sender);
	void __fastcall filterOffBtnClick(TObject *Sender);
	void __fastcall saveFileBtnClick(TObject *Sender);
	void __fastcall loadFileBtnClick(TObject *Sender);
	void __fastcall addAgeBtnClick(TObject *Sender);
	void __fastcall addMoneyClick(TObject *Sender);
	void __fastcall readOnlyGridChange(TObject *Sender);
	void __fastcall readOnlyTableChange(TObject *Sender);
	void __fastcall locateBtnClick(TObject *Sender);
	void __fastcall lookupBtnClick(TObject *Sender);
	void __fastcall sumBtnClick(TObject *Sender);

private: // User declarations
public: // User declarations
	__fastcall TForm1(TComponent* Owner);
};

// ---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
// ---------------------------------------------------------------------------
#endif
